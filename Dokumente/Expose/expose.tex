%%%%%%%%%%%%
% Präambel %
%%%%%%%%%%%%

	\documentclass[a4paper, 12pt]{scrartcl}

	% Schriftart / Font
	\usepackage{mathptmx}
	% Eineinhalbfacher Zeilenabstand
	\usepackage[onehalfspacing]{setspace}
	% Schriftart in Überschriften
	\setkomafont{disposition}{\normalcolor\bfseries}
	% Kein Einrücken nach Zeilenumbruch
	\setlength\parindent{0pt}

	% Umlaute direkt verwenden
	\usepackage[utf8]{inputenc}
	% Trennung von Wörtern mit Umlauten
	\usepackage[T1]{fontenc}

	% Seitenränder
	\usepackage[hmargin=2cm,vmargin=2.5cm,includefoot,footskip=1cm]{geometry}
	% verbesserter Randausgleich / besseres Schriftbild
	\usepackage{microtype}
	% Textausrichtung
	\usepackage{ragged2e}
	% Deutsche Anführungszeichen
	\usepackage[autostyle=true,german=quotes]{csquotes}
	% Deutsche Datumsangabe
	\usepackage[ngerman]{datetime}
	% deutsche Trennregeln
	\usepackage[ngerman]{babel}
	% Stil von enumerate-Listen
	\usepackage{enumitem}

	% Grafiken einbinden
	\usepackage{graphicx}
	% Textumfluss für Grafiken
	\usepackage{wrapfig}
	% Titel, Beschreibungen etc. von Bildern und Darstellungen
	\usepackage{caption}

	% Fußnoten
	\usepackage{bigfoot}
	\usepackage{float}
	\floatstyle{boxed}
	\restylefloat{figure}
	% PDF einbinden
	\usepackage{pdfpages}
	% Lange Tabellen (mit Seitenumbrüchen etc)
	\usepackage{longtable}
	% Hyperlinks (manuell + im Inhaltsverzeichnis)
	\usepackage{hyperref}
	\urlstyle{same}

	% Literaturverwaltung
	\usepackage[backend=biber, natbib, style=apa]{biblatex}
	% Einbinden der bib-Datei
	\addbibresource{/home/simeon/Dokumente/Uni/Masterarbeit/ma-repository/Literatur/literatur-masterarbeit.bib}

	% Blindtext einfügen
	\usepackage{blindtext}

	% Hurenkinder und Schusterjungen verhindern
	\clubpenalty10000
	\widowpenalty10000
	\displaywidowpenalty=10000

	% Inhaltliche Angaben zum Dokument
	\date{\today}
	\author{Simeon Schüz}
	\title{Exposé Masterarbeit}

%%%%%%%%%%%%%%%%%
% Ende Präambel %
%%%%%%%%%%%%%%%%%

\begin{document}

% Titelseite erstellen
	\maketitle
	\pagenumbering{gobble}
	% Keine Seitenzahl anzeigen

% Inhaltsverzeichnis
	\tableofcontents
	\newpage

% Arabische Seitenzahlen (und Counter auf 1)
	\pagenumbering{arabic}
	\pagestyle{plain}

% Inhalt

\section{Problembereich / Problemstellung}

Die automatische Verarbeitung von natürlicher Sprache, sei es \textit{Natural Language Processing} oder \textit{Natural Language Generation} verlangt nach einer Repräsentation der Bedeutungen von Sprachlichen Einheiten. Gängige Ansätze für die Repräsentation von Wortbedeutungen fußen auf der Annahme, dass die Bedeutung eines Wortes durch die Verteilung der Wörter in seiner Umgebung beschriben werden kann \parencite[670]{jurafskymartin2009}. Diese \textit{distributionellen} Ansätze sind aus computationeller Perspektive reizvoll, da bei ihnen extralinguistische Einflüsse nicht in die Betrachtung mit einbezogen werden müssen. Die Bedeutung eines Wortes wird aus der Kookurenz mit anderen Wörtern erschlossen, erschließt sich also gänzlich aus dem textuellen Kontext des betrachteten Wortes. Vereinfacht ausgedrückt wird in distributionellen Ansätzen die \textit{Ähnlichkeit} zwischen Begriffen erfasst: Wörter können etwa als Vektoren in einem hochdimensionalen Vektorraum dargestellt werden, in dem jede Dimension den Grad der Kookurrenz mit einem bestimmten anderen Wort repräsentiert. Die Ähnlichkeit zweier Wörter kann anschließend geometrisch bestimmt werden.

In distributionellen Ansätzen ist die Bedeutung eines Wortes folglich als Relation zu anderen Worten repräsentiert. Das macht diese Ansätze vor allem aus ökonomischer Sicht attraktiv: Wortbedeutungen können aus unannotierten Daten erlernt werden, so können Lexika von nahezu beliebiger Größe unter vertretbarem Aufwand aufgebaut oder erweitert werden.

Die Beschränkung auf textuelle Daten als Grundlage für das Erlernen von Wortbedeutungen kann jedoch auch ein beschränkender Faktor sein: Soll die maschinelle Sprachverarbeitung etwa die menschliche Kognition widerspiegeln, ist die Vernachlässigung extralinguistischer Reize kaum tragbar \parencite[2325 f.]{beinborn2018}. Zudem führen rein distributionelle Ansätze zu dem von Harnad beschriebenen \textit{Symbol Grounding Problem}: Inwiefern repräsentieren Modelle, in denen sich Symbole lediglich auf andere Symbole beziehen, Gegebenheiten in der wirklichen Welt? Werden Wortbedeutungen nur als Relationen zueinander beschrieben, führt dies zu einem zirklulären, in sich geschlossenen semantischen System, das von Referenten in der physischen Welt getrennt ist \parencite[vgl.][]{harnad1990}. Beide Probleme lassen sich nicht dadurch lösen, dass distributionelle Ansätze in ihrer Komplexität erweitert werden (indem etwa syntaktische Beziehungen zwischen den Wörtern mit einbezogen werden) - sie verlangen nach tatsächlichen Beziehungen zwischen sprachlichen Einheiten und der realen Welt.

\bigskip
\textit{Grounding} beschreibt diese Verbindung zwischen Sprache und physischer Welt \parencite[9]{richter2018}. Richter illustriert sprachliches Grounding anhand des Zusammenspiels zwischen \textit{Symbolen}, \textit{Konzepten} und \textit{Objekten}. Objekte sind Entitäten in der physischen Welt, die durch unser sensorisches System wahrgenommen werden können. Die einzelnen wahrnehmbaren Merkmale eines Objekts sind als Konzept des Objekts mental repräsentiert. Konzepte erlauben das Erkennen von Objekten, aber auch abstrahierende Leistungen wie das Herstellen von Ähnlichkeitsbeziehungen. Auf Objekte und Konzepte kann durch die Verwendung von Symbolen (wie etwa Wörtern) referiert werden. Symbole sind arbiträr, also in ihrer Form unabhängig von den Objekten und Konzepten, auf die sie verweisen \parencite[vgl.][10 f.]{richter2018}.

Grounding betrifft die Verbindung zwischen Symbolen, Konzepten und Objekten. Richter unterscheidet zwischen verschiedenen, komplementären Definitionen des Begriffs. Im Rahmen dieser Masterarbeit soll Grounding (angelehnt an \cite{harnad1990}) als die Verbindung zwischen Symbolen und Konzepten verstanden werden, die wiederum auf perzeptuellen Eindrücken beruhen.

Grounding ist per Definition nicht auf eine spezifische Modalität festgelegt. Die geplante Masterarbeit soll sich auf \textit{visuelles Grounding} beschränken, das sich auf die visuelle Modalität bezieht und demnach das Grounding von Wortbedeutungen auf der Basis optischer Eindrücke zum Ziel hat. Die Verbindung zwischen sprachlicher und extralinguistischer visueller Information macht visuelles Grounding zu einem hochgradig interdisziplinären Feld, das unter anderem eine Schnittmenge zwischen Linguistik, maschineller Bildverarbeitung und Kongnitionswissenschaft darstellt.
\bigskip

Visuelles Grounding soll in der geplanten Masterarbeit am Beispiel von Farbbegriffen untersucht werden. Dazu gibt es einige vorausgehende Untersuchungen. Im überwiegenden Teil dieser Arbeiten wurden jedoch keine realistischen Daten als Grundlage gewählt - zumeist wird auf Farbkarten, arrangierte Versuchsaufbauten oder andere kontrollierte Quellen zurückgegriffen, um auf dieser Grundlage isolierte Farbreize auf entsprechende Farbbegriffe abzubilden. Dies führt zu einigen offenen Fragen, die im Fokus der geplanten Masterarbeit liegen: Wie kann beim Grounding mit natürlichem, möglicherweise störungsbehafteten Input umgegangen werden? Und was gilt es zu beachten, wenn die Farbe von konkreten Objekten bestimmt werden soll, anstatt von abstrakten Farbreizen?

Theoretische Relevanz hat das Thema für eine Reihe von Forschungsfeldern. Es stellt etwa eine konkrete Umsetzung des Appells von \citet[491]{witzelgegenfurtner2018} dar, Farbe verstärkt als Eigenschaft von Objekten, und nicht nur als Lichteigenschaft zu betrachten. Die Kodierung von relevanten Objekteigenschaften ist unter anderem notwendig für die Repräsentation von Farbbegriffen in \textit{Conceptual Spaces}. Gleichzeitig tangiert die Arbeit etwa mit dem Symbol Grounding Problem grundsätzliche Fragestellungen der computerlinguistischen Semantik, und widmet sich dem Grounding auf Grundlage von natürlichen Input-Quellen.

Praktische Relevanz hat die Fragestellung etwa durch mögliche Umsetzungen in multimodalen Dialogsystemen, die mittels Sprachproduktion auf extralinguistische, visuelle Reize reagieren.

\section{Zielsetzung und Erkenntnisinteresse}

In der geplanten Masterarbeit soll das Grounding von Farbwörtern untersucht werden. Ein umfangreiches Korpus von annotierten, natürlichen Bildern soll als Datengrundlage für ein Modell dienen, das auf der Basis von perzeptuellen Merkmalen Farbbezeichnungen für annotierte Objekte vorhersagen soll.

Sowohl die Wahl der Datengrundlage als auch die Fokussierung auf Objektfarben stellen nennenswerte Herausforderungen dar. So ist anzunehmen, dass natürliche Bilder, die ohne Vorselektion vom Modell verwendet werden, störungsbehafteter sind als Farbkarten, zum Zweck der Untersuchung ausgewählte Fotografien oder anderweitig kontrollierte Input-Materialien. Zu erwarten ist etwa eine große Bandbreite an unterschiedlichen Belichtungssituationen, ebenso wie untypische Vertreter von Objektklassen. Auch Aufnahme- oder Kompressionsartefakte oder fehlerbehaftete Annotationen sind nicht auszuschließen.

\bigskip
Der Fokus der Arbeit soll besonders auf dem Aspekt der Objektfarben liegen.
Bereits in Studien mit kontrolliertem Input wurde nachgewiesen, dass Farbbezeichnungen flexibel und kontextabhängig gebraucht werden \citep[vgl. etwa][]{baumgaertner2012, meo2014}.

Im Vergleich zur Benennung von isolierten Farbreizen die Verbindung zwischen perzeptuellen Merkmalen und sprachlichen Symbolen bei der Benennung von Objektfarben weniger linear. So sind die wenigsten Objekte vollkommen einfarbig - die meisten Objekte haben eine intrinsische Struktur und sind farblich komplex.
Vorausgehende Studien \citep[vgl. etwa][]{witzelgegenfurtner2018} legen zudem wechselseitige Effekte zwischen Objekttyp und wahrgenommer Objektfarbe nahe. Insbesondere sind diese für \textit{Color Diagnostic Objects} nachgewiesen - Objekte, die eine spezifische Eigenfarbe (die jeweilige \textit{Memory Color}) haben \citep[vgl.][]{westerbeek2014}. So wurde gezeigt, dass die Wahrnehmung einer Objektfarbe durch Top-Down-Prozesse vom Weltwissen über das betreffende Objekt und seine kanonische Farbe beeinflusst wird \citep[vgl.][]{hansen2006}. In der Theorie könnten aus dieser perzeptuellen Verschiebung unterschiedliche, objektabhängige Farbbezeichnungen für denselben Farbreiz folgen.
\bigskip

Das Einbeziehen des jeweiligen Objekttyps ist für ein \textit{Color Naming}-Modell demnach aus mehrerlei Gründen wünschenswert: Einerseits ist durch die Berücksichtigung des Objekttyps eine erhöhte Genauigkeit bei der Farbbenennung zu erwarten, da das Modell auf diese Weise robust gegenüber gewissen Farbschwankungen gestaltet werden kann. \citet[1368]{hansen2006} vermuten etwa, dass bei Color Diagnostic Objects die Voraktivierung der jeweiligen Memory Color das Erkennen der Objektfarbe in variierenden Lichtsituationen erleichtert, und somit zur \textit{Color Constancy} beiträgt. Ein solcher Effekt könnte der Genauigkeit eines Modell für die Bennenung von Objektfarben zuträglich sein - zumal ein ähnlicher Effekt auch für andere Störfaktoren als Belichtungseinflüsse möglich ist. Des Weiteren ist ein Modell, das beim Color Naming den Typ des betrachteten Objekts nicht einbezieht, aus kognitiver Perspektive wenig plausibel: Der \textit{Memory Color Effect}, die mentale Voraktivierung der charakteristischen Objektfarbe, kann beispielsweise nicht modelliert werden, wenn ausschließlich Farbwerte betrachtet werden.
\bigskip

Das Ziel der Masterarbeit ist demnach die Modellierung von Abhängigkeiten zwischen Objekten und Farbattributen. Dies schließt sowohl theoretische als auch praktische Aspekte mit ein.

Aus semantischer Sicht stellt sich die Frage, ob die Relation zwischen Objekten und Farbattributen immer dieselbe ist, oder es erfassbare Unterschiede gibt, etwa zwischen kanonischen Eigenfarben von Color Diagnostic Objects, Eigenfarben von Objekten ohne kanonische Farbe und kontextuell bedingten Objektfarben (etwa durch externe Lichteinflüsse).

In Hinsicht auf die Implementierung in ein Modell für Farbbenennung soll untersucht werden, ob sich ein Effekt im Sinne der Color Constancy auf Grundlage der statistischen Farbpräferenzen von Objekttypen erzielen lässt. Zudem soll untersucht werden, ob es im Korpus signifikante Unterschiede zwischen den betrachteten Objekttypen beim Mapping von visuellen Low-Level-Merkmalen auf Farbbegriffe gibt, und wie diese objektspezifischen Abweichungen durch ein Modell erfasst werden.

Diese Untersuchungen schließen die Frage nach einer geeigneten Kodierung für Objekteigenschaften für die Implementierung eines Color Naming-Modells mit ein. Durch den Vergleich verschiedener Ansätze der Kodierung soll die geeignetste Methode herausgestellt werden. Berücksichtigt werden sollen sowohl Ansätze zur expliziten Kodierung von speziellen Objekttypen als auch zur Kodierung von (möglicherweise unterspezifizierten) Objekteigenschaften.

Zusammengefasst besteht das Erkenntnisinteresse in den folgenden Bereichen:

\begin{itemize}
	\item Grounding auf Grundlage von natürlichem/störungsbehafteten Input
	\item Grounding von Objektfarben statt abstrakten Farbreizen
	\item Farbe als Objekteigenschaft
	\item Repräsentation / Kodierung von Objekteigenschaften
	\item Implementierung von Objektrepräsentationen in ein Modell für Farbbenennung
\end{itemize}

\section{Forschungsstand und theoretische Grundlage}

\subsection{Theoretische Grundlage}

\paragraph{Farbe und Farbkategorien}

Durch den Fokus auf Farbwörtern als Kategorien von perzeptuell wahrgenommenen Farbreizen und die Methode der Implementierung, die unter anderem die Berechnung von Histogrammen mit einschließt, sollen die Grundlagen des menschlichen Farbensehen und der Kodierung von Farbinformationen in den theoretischen Teil der Arbeit aufgenommen werden. Grob umfasst dies die folgenden Punkte:

\begin{itemize}
	\item Physikalische und physiologische Grundlagen der menschlichen Farbwahrnehmung: Was ist Farbe im physikalischen Sinn? Lichtfarbe vs. Körperfarbe? Wie funktioniert die menschliche Farbwahrnehmung? Physiologisch / psychologisch bedingte Phänomene wie Color Constancy oder Spacial Averaging, die einen Einfluss auf die Bildung von Farbkategorien oder die Bestimmung von Objektfarben haben

	\item Wichtige Farbmodelle wie RGB, HSV, L*a*b* (insofern eine Unterscheidung dieser Modelle von Relevanz ist)

	\item Kategorienbildung in Bezug auf Farbe: Einleitung über verschiedene Arten der Farbkategorisierung (Munsell...), Farbtermini als Methode, den spektralen Farbverlauf in Kategorien einzuteilen, Eigenschaften von Farbkategorien (Fuzzy Categories, Graded Membership, eher durch Prototypen als klare Grenzen definiert), Kontext- und Objektabhängigkeit von Farbbezeichnungen, Semantische Eigenschaften von Farbbezeichnungen (Vagheit, Wortbedeutungen als Wahrscheinlichkeitsverteilungen über Wörter)

	\item Exemplarische Literaturquellen:
	 \cite{witzelgegenfurtner2018}, \cite{viethen2012}, \cite{westerbeek2014}, \cite{tarenskeen2015}, \cite[3]{meo2014}, \cite{williamson1996}
\end{itemize}

\paragraph{Grounding}

Im Theorieteil der Arbeit sollen die Grundzüge von sprachlichem Grounding erläutert werden. Dies umfasst unter anderem das Beschreiben von Defiziten gängiger Ansätze zur Bedeutungsrepräsentation sprachlicher Ausdrücke, die im Wesentlichen als Harnads \textit{Symbol Grounding Problem} zusammengefasst sind. Die Definition des Begriffs \textit{Grounding} soll einhergehen mit der Unterscheidung zwischen verschiedenen Ausprägungen von Grounding (konversationelles Grounding, perzeptuelles Grounding etc.).

\begin{itemize}
	\item Exemplarische Literaturquellen: \cite{harnad1990}, \cite{lammens1994}, \cite{richter2018}, \cite{beinborn2018}, \cite{clarkbrennan1991}, \cite{clarkwilkesgibbs1986}
\end{itemize}


\subsection{Forschungsstand}

\subsubsection{Vorausgehende Versuche}

Im Bereich des visuellen Groundings werden Farbbegriffe häufig als Beispiel herangezogen \parencite[vgl.][246]{zarriessschlangen2016}. Dementsprechend gibt es einige Studien, die sich mit dem Grounding von Farbtermini befassen.
\bigskip

\cite{mojsilovic2005} entwickelt in ihrem Paper ein Modell für Farbkategorisierung und -benennung sowie die Bestimmung von Farben in einem komplexen Bild. Ausgangspunkt für ihre Untersuchung bilden die Farbnamen aus dem \textit{ISCC–NBS System of Color Designation}, aus denen eine allgemeine Syntax für Farbbezeichnungen abgeleitet wird.
Das Modell von \citeauthor{mojsilovic2005} basiert Erkenntnissen über die neurophysiologische Verarbeitung von Farbwörtern in der menschlichen Wahrnehmung. Im Kern des Modells steht ein Algorithmus, der einem arbiträren Farbreiz einen Terminus zuweist. Hierzu wird die Distanz zwischen dem Farbreiz und eines benannten Farb-Prototypen ermittelt. Das Modell wird durch mehrere Experimente und auf der Grundlage von natürlichen Bildern evaluiert.
Um natürliche Bilder analysieren zu können, werden die Bilder durch Methoden vorverarbeitet, die der menschlichen Farbwahrnehmung zugrundeliegenden Prozessen nachempfunden sind (\textit{Color Constancy} und \textit{Spatial Averaging}). Für jeden Farbbereich im vorverarbeiteten Bild wird anschließend ein entsprechender Terminus gesucht.

Bei der Evaluation auf Grundlage von natürlichen Bildern berücksichtigt \citeauthor{mojsilovic2005} zwar Kontexteffekte wie variierende Belichtungssituationen, nicht aber die Typen der Objekte, deren Farbe beschrieben werden. Die untersuchten Bilder werden lediglich als Ansammlung von Farbbereichen betrachtet. Der Anforderung von \cite{witzelgegenfurtner2018}, dass Farbe auch als Objekteigenschaft verstanden werden muss, wird also nicht entsprochen. Generell beschränkt sich \citeauthor{mojsilovic2005} auf visuellen Input, und bezieht in ihrem Modell keine darüber hinausgehenden Informationen mit ein.
\bigskip

\citeauthor{baumgaertner2012} beschreiben Algorithmen für das Erkennen und Generieren von Farbbeschreibungen. Die Arbeit beginnt mit der Beobachtung, dass die Sprecher einer Sprache nicht notwendigerweise dasselbe Lexikon oder dieselben semantischen Repräsentationen teilen. In Gesprächssituationen wird diesem individuelle Sprachgebrauch durch interaktive Grounding-Prozesse zwischen den Gesprächsteilnehmern sowie durch weniger rigide Interpretationen von Wortbedeutungen begegnet. \citeauthor{baumgaertner2012} versuchen eine flexible semantische Repräsentation am Beispiel von Farbwörtern zu entwickeln, die sowohl an Kontextgegebenheiten als auch an die individuelle Wortwahl von Gesprächspartnern angepasst werden kann. So soll ermöglicht werden, dass ein Agent für visuelle Stimuli Farbbegriffe wählen kann, die nicht spezifischer oder komplexer sind als kontextuell erforderlich, und somit den Grice'schen Konversationsmaximen entsprechen. Zudem soll sich die Wahl der Farbbegriffe an der Wortwahl des jeweiligen Gesprächspartners orientieren.
Als Datengrundlage wählen die Autoren eine Datenbank, in der RGB-Werte mit Farbtermini verbunden werden. Das Zusammenspiel von zwei Algorithmen soll die Interpretation und Generierung von Farbbeschreibungen ermöglichen: Der Interpretations-Algorithmus \textit{ALIN} vergleicht jede Farbe in einer präsentierten Szene mit dem RGB-Wert einer Referenzfarbe aus der Datenbank. Der Generierungs-Algorithmus \textit{GENA} berechnet einen möglichst grundlegenden Farbterminus für ein gegebenes Farb-Target. Beim Vergleichen von Farbwerten wird auf das Konzept von \textit{Conceptual Spaces} zurückgegriffen.
Die Evaluation der Algorithmen durch Experimente mit menschlichen Versuchspersonen zeigt, dass brauchbare Ergebnisse im Erkennen von beschriebenen Target-Items erzielt werden. Die Evaluation von automatisch generierten Farbbegriffen gestaltet sich als schwierig und wird von den Autoren nicht abschließend durchgeführt.

Die Arbeit von \cite{baumgaertner2012} ist aufschlussreich bezüglich der flexiblen Semantik von Farbbegriffen. In Bezug auf die Benennung von Objektfarben wird keine Aussage getroffen, alle Experimente werden auf Grundlage von abstrakten Farbitems durchgeführt.
\bigskip

\citeauthor{meo2014} entwickeln eine Methode für die Differenzierung zwischen Farben durch die Verwendung von Farbwörtern. Den linguistischen Rahmen stellen aufgrund der variablen und kontextabhängigen Grenzen zwischen Farbwörtern Theorien über sprachliche Vagheit dar. Um dieser Vagheit zu entsprechen entwickeln die Autoren ein kognitives Modell, aufbauend auf der Bayesschen \textit{rational analysis}. Konkret versuchen \citeauthor{meo2014} in ihrer Arbeit die Wahrscheinlichkeit zu modellieren, dass ein Sprecher einen bestimmten Farbbegriff benutzt, in Abhängigkeit von Kontextreizen und der Frequenz des betrachteten Farbwortes.
Der zentrale Algorithmus bei \cite{meo2014} stellt das \textit{Y-but-not-Z confidence rating} dar: Für alle Farbtermini wird die Wahrscheinlichkeit berechnet, mit welcher der Terminus Farbe Y beschreibt und Farbe Z ausschließt. Der Algorithmus operiert auf der Grundlage des \textit{LUX}-Modells (\textit{Lexicon of Uncertain Color Standards}), in dem Farbbeschreibungen probabilistisch mit Regionen im HSV-Farbraum verknüpft werden. Zusätzlich wird die Salienz eines Begriffs mit einbezogen (erfasst als das Maß wie oft ein Farbbegriff benutzt wird, wenn er eine Farbe adäquat beschreibt).
Das Modell von \cite{meo2014} erzielt sehr gute Ergebnisse im Bestimmen der Referenten von Farbbeschreibungen im Kontext von Distraktor-Farbreizen. Systematische Fehler finden die Autoren nicht, das Modell scheint nur in solchen Fällen fehlerhaft zu agieren, in denen auch Menschen vor Probleme gestellt werden.

Die Arbeit von \cite{meo2014} ist von Relevanz bezüglich der Formalisierung von vagen und flexibel eingesetzten Farbbegriffen. Da das Modell auf der Grundlage von Farbtafeln entworfen wird, teffen die Autoren keine Aussage über die Bestimmung von Farbbegriffen auf der Grundlage natürlicher Bilder oder die Benennung von Objektfarben.
\bigskip

Im Unterschied zu den bislang erwähnten Studien untersuchen \citeauthor{zarriessschlangen2016} das Grounding von Farbwörtern für Objekte auf der Grundlage von natürlichen Fotografien. Einleitend werden diverse Herausforderungen beschrieben, die sich durch die gewählte Datengrundlage sowie den Fokus auf Objektfarben ergeben - zusätzlich zu der ohnehin flexiblen und kontextabhängigen Nutzung von Farbwörtern stellt sich die Frage, wie mit dem komplexen Aufbau von Objekten,den Belichtungskontexten und der Integration von Top-Down-Wissen umgegangen werden kann.
In einer Reihe von Versuchen werden unterschiedliche Klassifikatoren trainiert. Den ersten Schritt stellt ein allgemeiner visueller Klassifikator dar, der Farbhistogramme von Objekten auf Farbwörter abbildet. Die AutorInnen beobachten, dass die Vorhersagen des Klassifikators genauer werden, wenn die Entropie der prognostizierten Wahrscheinlichkeiten für die einzelnen Farbbegriffe gering ist, der Klassifikator also einen Farbbegriff als klaren Favoriten ausmachen kann. Objektspezifische Farbtendenzen gibt der Klassifikator nicht wieder.
Um diese abzubilden, werden für die Objekttypen im Untersuchungskorpus separate Klassifikatoren trainiert. Die Evaluation dieser Klassifikatoren offenbart schlechtere Ergebnisse als beim allgemeinen visuellen Klassifikator. Interpretiert wird dieses Ergebnis dahingehend, dass die prototypische Bedeutung der einzelnen Farbwörter nicht erlernt werden.
Als dritter Versuch werden Rekalibrierungsklassifikatoren trainiert: Diese sollen systematische, objektspezifische Abweichungen von den Ergebnissen des allgemeinen Klassifikators vorhersagen. Insbesondere Rekalibrierungsklassifikatoren, die sowohl die Objekttypen als auch den Kontext mit einbeziehen, verbessern das Ergebnis des allgemeinen Klassifikators. Klassifikatoren in diesem Versuch verhalten sich konservativer als objektspezifische Klassifikatoren, ändern also die Prognose des allgemeinen Klassifikators vor allem dann, wenn die Klassifikation auf Grundlage der Farbhistogramme zu ambivalenten Vorhersagen führt.
\bigskip

Das Paper von \cite{zarriessschlangen2016} entspricht am nächsten der Ausrichtung der geplanten Masterarbeit, und dient daher als inhaltlicher und methodologischer Ausgangspunkt. Die AutorInnen präsentieren relevante Inhalte zum Grounding auf der Grundlage von natürlichem Input sowie zum Grounding von Objektfarben. Unterschiedliche Ansätze zur Objektrepräsentation werden nicht verglichen.

\section{Konzept}
%Fragestellung, Hypothese (Ergebnisannahme), Methodik

\subsection{Arbeitshypothesen}

\begin{enumerate}
	\item Objektinformationen verbessern die Performance von Color-Naming-Modellen
	\begin{enumerate}
		\item Der Effekt wird besonders deutlich für Color Diagnostic Objects, da diese zumeist mit einer speziellen Farbe auftreten (ihrer Memory Color)
	\end{enumerate}
	\item Unterschiede zwischen verschiedenen Methoden der Kodierung von Objekteigenschaften führen zu unterschiedlichen Resultaten beim Color Naming
	\begin{enumerate}
		\item CNN-Features generalisieren: Sie haben Einfluss auf die Vorhersage von Farbwörtern für Objekttypen, mit denen sie nicht trainiert wurden
		\item CNN-Features auf unterschiedlicher Ebene führen zu Performance-Unterschieden
	\end{enumerate}
\end{enumerate}

\subsection{Methode}

\begin{itemize}
	\item Korpusstudie / Datenexploration: Extrahieren von Objekten mit Farbattributen, Gruppieren in Objekte mit oder ohne kanonische Eigenfarbe
	\item Pre-Test: Objektspezifische Farbtendenzen (Verteilung Farbwörter / Objekt, RGB-Werte / Farbwort und Objekt)
	\item Baseline: Allgemeiner Klassifikator, angelehnt an \cite{zarriessschlangen2016}. Dient als Vergleichswert für nachfolgende Untersuchungen und zur erwähnten Arbeit von \citeauthor{zarriessschlangen2016}.
	\item Objektspezifische Modelle: Integration von Objektinformationen verschiedener Arten (explizite Objektinformationen, implizite Objektinformationen)
	\item Evaluation unter verschiedenen Bedingungen: Allgemeines Sample, Color Diagnostic Objekts, Objekte ohne kanonische Eigenfarbe
	\item Interpretation der Ergebnisse
\end{itemize}


\subsection{Material}
	Als Datengrundlage soll das annotierte Bildkorpus \textit{Visual Genome} verwendet werden \parencite{krishnavisualgenome}. Die Vorverarbeitung des Materials wird hauptsächlich mithilfe der Python-Bibliotheken \textit{Pandas} und \textit{OpenCV} durchgeführt. Diese Vorverarbeitung umfasst unter anderem das Sammeln von Objektinstanzen mit dazugehörigen Farbbeschreibungen, das Kategorisieren von Objekttypen nach den mit ihnen assoziierten Farbattributen, sowie das extrahieren von Farbhistogrammen für die einzelnen Objektinstanzen.

\section{Vorläufige Gliederung}

	\begin{enumerate}[label*=\arabic*.]
		\item Einführung

		\item Theoretischer Hintergrund
		\begin{enumerate}[label*=\arabic*.]
			\item Farbe
			\begin{enumerate}[label*=\arabic*.]
				\item Physikalische und physiologische Grundlagen
				\begin{enumerate}[label*=\arabic*.]
					\item Was ist Farbe?
					\item Menschliche Farbwahrnehmung
					\item Maschinelle Implementierung
				\end{enumerate}
				\item Farbe in der Linguistik
				\begin{enumerate}[label*=\arabic*.]
					\item Historische Übersicht
					\item Semantik von Farbwörtern
				\end{enumerate}
				\item Farbe als Objekteigenschaft
			\end{enumerate}

			\item Grounding
			\begin{enumerate}[label*=\arabic*.]
				\item Motivation
				\item Begriffsklärung / Unterscheidung
			\end{enumerate}
		\end{enumerate}

		\item Versuche
		\begin{enumerate}[label*=\arabic*.]
			\item Hintergrund und Hypothese
			\item Materialgrundlage
			\begin{enumerate}[label*=\arabic*.]
				\item Visual Genome
				\item Exemplarisches Darstellen des betrachteten Effekts
				\item Preprocessing / Datenextraktion
			\end{enumerate}
			\item Methode
			\item Ergebnisse
		\end{enumerate}

		\item Diskussion

		\item Fazit und Ausblick

		\item Bibliographie
	\end{enumerate}
%
%\section{Arbeits-/Zeitplan}
%
%\paragraph{Schritte für Modellentwicklung}
%%(aus %https://morioh.com/p/b56ae6b04ffc/a-complete-machine-learning-proje%ct-walk-through-in-python)
%\begin{enumerate}
%	\item Data cleaning and formatting
%		\begin{itemize}
%			\item Format der Datengrundlage?
%			\item Fehlende Werte?
%		\end{itemize}
%	\item Exploratory data analysis
%		\begin{itemize}
%			\item Werteverteilung für Target
%			\item Relationships zwischen Features und Target
%		\end{itemize}
%	\item Feature engineering and selection
%		\begin{itemize}
%			\item Feature engineering: Features aus Rohdaten extrahieren oder erstellen (Transformationen, One-Hot-Encoding für Kategorien)
%			\item Feature selection: Relevante Features auswählen, unnötige Features (redundant oder ohne Effekt) entfernen
%		\end{itemize}
%	\item Establishing a baseline
%		\begin{itemize}
%			\item Split zwischen Trainings- und Testset
%			\item Maßstab für Performance des Modells
%		\end{itemize}
%	\item Compare several machine learning models on a performance metric
%		\begin{itemize}
%			\item Accuracy als Maßstab
%		\end{itemize}
%	\item Perform hyperparameter tuning on the best model
%		\begin{itemize}
%			\item underfitting vs. overfitting
%		\end{itemize}
%	\item Evaluate the best model on the testing set
%	\item Interpret the model results
%	\item Draw conclusions and document work
%\end{enumerate}

% Grober Zeitplan für das Anfertigen der Arbeit

% Literaturverzeichnis

\newpage
\phantomsection
\addcontentsline{toc}{section}{Literatur}
\printbibliography
\end{document}
