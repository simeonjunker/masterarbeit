% Encoding: UTF-8

@InProceedings{houghschlangen2017,
  author    = {Hough, Julian and Schlangen, David},
  title     = {{It's Not What You Do, It's How You Do It: Grounding Uncertainty for a Simple Robot}},
  booktitle = {Proceedings of the 2017 Conference on Human-Robot Interaction (HRI2017)},
  year      = {2017},
}

@InProceedings{schlangen2016,
  author    = {Schlangen, David},
  title     = {{Grounding, Justification, Adaptation: Towards Machines That Mean What They Say}},
  booktitle = {Proceedings of the 20th Workshop on the Semantics and Pragmatics of Dialogue (JerSem)},
  year      = {2016},
}

@InProceedings{schlangen2016b,
  author    = {Schlangen, David and Zarrieß, Sina and Kennington, Casey},
  title     = {{Resolving References to Objects in Photographs using the Words-As-Classifiers Model}},
  booktitle = {Proceedings of the 54th Annual Meeting of the Association for Computational Linguistics (ACL 2016)},
  year      = {2016},
}

@InProceedings{zarriessschlangen2016,
  author    = {Zarrieß, Sina and Schlangen, David},
  title     = {{Towards Generating Colour Terms for Referents in Photographs: Prefer the Expected or the Unexpected?}},
  booktitle = {Proceedings of the 9th International Natural Language Generation conference},
  year      = {2016},
}

@Article{krahmerdeemter2012,
  author   = {Krahmer, Emiel and Deemter, Kees van},
  title    = {Computational Generation of Referring Expressions: A Survey},
  journal  = {Computational Linguistics},
  year     = {2012},
  volume = {38},
  number = {1},
}

@InCollection{clarkbrennan1991,
  author    = {Herbert H. Clark and Susan E. Brennan},
  title     = {Grounding in Communication},
  booktitle = {Perspectives on Socially Shared Cognition},
  publisher = {American Psychological Association},
  year      = {1991},
  editor    = {L. B. Resnick and J. M. Levine and S. D. Teasley},
  pages     = {127-149},
  location  = {Washington DC},
}

@Article{clarkwilkesgibbs1986,
  author   = {Herbert H. Clark and Deanna Wilkes-Gibbs},
  title    = {Referring as a collaborative process},
  journal  = {Cognition},
  year     = {1986},
  volume   = {22},
  number   = {1},
  pages    = {1 - 39},
}

@Article{royreiter2005,
  author   = {Deb Roy and Ehud Reiter},
  title    = {Connecting language to the world},
  journal  = {Artificial Intelligence 167, 1-2, 1},
  year     = {2005},
  volume = {167},
  number = {1-2},
  pages = {1-12},
}

@Article{roy2002,
  author   = {Deb Roy},
  title    = {Learning visually grounded words and syntax for a scene description task},
  journal  = {Computer speech \& language},
  year     = {2002},
  volume   = {16},
  number   = {3-4},
}

@InProceedings{schlangenzarriesskennington2016,
  author   = {Schlangen, David and Zarrieß, Sina and Kennington, Casey},
  title    = {Resolving References to Objects in Photographs using the Words-As-Classifiers Model},
  booktitle  = {Proceedings of the 54th Annual Meeting of the Association for Computational Linguistics (ACL 2016)},
  year     = {2016},
}

@Article{monroeetal2017,
  author   = {Monroe, Will and Hawkins, Robert X.D. and Goodman, Noah D. and Potts, Christopher},
  title    = {Colors in Context: A Pragmatic Neural Model for Grounded Language Understanding},
  journal  = {Transactions of the Association for Computational Linguistics},
  year     = {2017},
  volume   = {5},
  pages    = {325-338},
}

@article{harnad1990,
author = "Stevan Harnad",
title = "The symbol grounding problem",
journal = "Physica D: Nonlinear Phenomena",
volume = "42",
number = "1",
pages = "335-346",
year = "1990",
}

@InCollection{plebe2014,
  author       = "Plebe, Alessio and De La Cruz, Vivian",
  title        = "Color seeing and speaking: Effects of biology, environment and language",
  pages        = "291-306",
  year         = "2014",
  booktitle = {Colour Studies: A broad spectrum},
  publisher = {American Psychological Association},
  editor    = {Wendy Anderson and Carole P. Biggam and Carole Hough and Christian Kay},
  publisher = {John Benjamins Publishing Company},
  location  = {Amsterdam},
}

@InProceedings{beinborn2018,
  author = 	"Beinborn, Lisa
		and Botschen, Teresa
		and Gurevych, Iryna",
  title = 	"Multimodal Grounding for Language Processing",
  booktitle = 	"Proceedings of the 27th International Conference on Computational Linguistics",
  year = 	"2018",
}

@MISC{lammens1994,
    author = {Johan Maurice Gisele Lammens},
    title = {A Computational Model of Color Perception and Color Naming},
    year = {1994}
}

@InProceedings{viethen2012,
  author        = {Viethen, Jette and Goudbeek, Martijn and Krahmer, Emiel},
  title         = {The Impact of Colour Difference and Colour Codability on Reference Production},
  year          = {2012},
  booktitle     = {Proceedings of the Cognitive Science Society},
}

@article{viethen2017,
  author       = {Viethen, Jette and Vessem, Thomas and Goudbeek, Martijn and Krahmer, Emiel},
  title        = {Color in Reference Production: The Role of Color Similarity and Color Codability},
  year         = {2017},
  journal      = {Cognitive Science},
  volume       = {41},
  number       = {6},
  pages       = {1493–1514},
}

@inproceedings{krishnavisualgenome,
  title={Visual Genome: Connecting Language and Vision Using Crowdsourced Dense Image Annotations},
  author={Krishna, Ranjay and Zhu, Yuke and Groth, Oliver and Johnson, Justin and Hata, Kenji and Kravitz, Joshua and Chen, Stephanie and Kalantidis, Yannis and Li, Li-Jia and Shamma, David A and Bernstein, Michael and Fei-Fei, Li},
  year = {2016},
}

@book{jurafskymartin2009,
  author       = {Jurafsky, Dan and Martin, James H.},
  title        = {Speech and language processing},
  edition      = {Pearson internat. ed., 2. ed.},
  address      = {Upper Saddle River, NJ},
  publisher    = {Pearson Prentice Hall},
  year         = {2009},
  series       = {Prentice Hall series in artificial intelligence},
}

@InProceedings{westerbeek2014,
  author       = {Westerbeek, H.G.W. and Koolen, R.M.F. and Maes, A.A.},
  editor       = {Bello, Paul and Guarini, Marcello and McShane, Marjorie and Scassellati, Brian},
  title        = {On the role of object knowledge in reference production:Effects of color typicality on content determination},
  language     = {eng},
  year         = {2014},
  booktitle      = {CogSci 2014 : Cognitive Science Meets Artificial Intelligence: Human and Artificial Agents in Interactive Contexts},
}

@InProceedings{tarenskeen2015,
  author       = {Tarenskeen, S.L. and Broersma, M. and Geurts, B.},
  editor       = {Larsson, S. and Howes, C.},
  title        = {'Hand me the yellow stapler' or 'Hand me the yellow dress': Colour overspecification depends on object category},
  year         = {2015},
  booktitle    = {Proceedings of the 19th Workshop on the Semantics and Pragmatics of Dialogue (SemDial; goDIAL)},
  pages = {140-148},
}

@article{mojsilovic2005,
author = {Mojsilovic, Aleksandra},
year = {2005},
pages = {690-699},
title = {A Computational Model for Color Naming and Describing Color Composition of Images},
volume = {14},
number = {5},
journal = {IEEE Transactions on Image Processing},
}

@phdthesis{richter2018,
  author      = {Mathis Marius Richter},
  title       = {A neural dynamic model for the perceptual grounding of spatial and movement relations},
  type        = {doctoralthesis},
  school      = {Ruhr-Universit{\"a}t Bochum, Universit{\"a}tsbibliothek},
  year        = {2018},
}

@InProceedings{baumgaertner2012,
  author = 	"Baumgaertner, Bert
		and Fernandez, Raquel
		and Stone, Matthew",
  title = 	"Towards a Flexible Semantics: Colour Terms in Collaborative Reference Tasks",
  booktitle = 	"*SEM 2012: The First Joint Conference on Lexical and Computational Semantics",
  year = 	"2012",
  pages = 	"80-84",
}

@InProceedings{meo2014,
  author = 	"Meo, Timothy
		and McMahan, Brian
		and Stone, Matthew",
  title = 	"Generating and resolving vague color references",
  booktitle = 	"Proceedings of the 18th Workshop Semantics
  and Pragmatics of Dialogue (SemDial)",
  year = 	"2014",
  pages = 	"107-115",
}

@book{williamson1996,
Author = {Williamson, Timothy},
Publisher = {Routledge},
Series = {Problems of Philosophy},
Title = {Vagueness},
Year = {1996},
}

@article{hansen2006,
  title={Memory modulates color appearance},
  author={Thorsten Hansen and Maria Olkkonen and Sergio Walter and Karl R. Gegenfurtner},
  journal={Nature Neuroscience},
  year={2006},
  volume={9},
  pages={1367-1368}
}

@Article{tanaka1999,
author="Tanaka, James W.
and Presnell, Lynn M.",
title="Color diagnosticity in object recognition",
journal="Perception {\&} Psychophysics",
year="1999",
volume="61",
number="6",
pages="1140-1153",
}

@article{witzelgegenfurtner2018,
author = {Witzel, Christoph and Gegenfurtner, Karl R.},
title = {Color Perception: Objects, Constancy, and Categories},
journal = {Annual Review of Vision Science},
volume = {4},
number = {1},
pages = {475-499},
year = {2018},
    }

@book{birbaumer2010,
  author       = {Birbaumer, Niels-Peter and Schmidt, Robert F.},
  title        = {Biologische Psychologie},
  edition      = {7., überarb. und erg. Aufl.},
  address      = {Heidelberg},
  publisher    = {Springer},
  year         = {2010},
  series       = {Springer-Lehrbuch},
}

@book{kueppers2012,
  author       = {Küppers, Harald},
  title        = {Farbenlehre},
  language     = {ger},
  edition      = {2. Aufl.},
  address      = {Köln},
  publisher    = {DuMont},
  year         = {2012},
}

@book{welsch2012,
  author       = {Welsch, Norbert and Liebmann, Claus Christian},
  title        = {Farben},
  edition      = {3., verb. und erw. Aufl.},
  address      = {Heidelberg},
  publisher    = {Spektrum Akademischer Verlag},
  year         = {2012},
}

@book{zollinger1999,
  author       = {Zollinger, Heinrich},
  title        = {Color},
  address      = {Zürich},
  publisher    = {Helvetica Chimica Acta},
  year         = {1999},
}

@article{schnabel1999,
  author       = {Schnabel, Gisela},
  editor       = {Schirmbacher, Peter},
  title        = {Die Farbmodelle HSV und HLS - Widersprüche in Theorie und Praxis},
  publisher    = {Humboldt-Universität zu Berlin},
}

@book{luebbe2013,
  author       = {Lübbe, Eva},
  title        = {Farbempfindung, Farbbeschreibung und Farbmessung},
  address      = {Wiesbaden},
  publisher    = {Springer Vieweg},
  year         = {2013},
  series       = {Praxis},
}

@article{levkowitz1993,
  author       = {Levkowitz, H. and Herman, G.T.},
  title        = {GLHS: A Generalized Lightness, Hue, and Saturation Color Model},
  year         = {1993},
  volume = {55},
  number = {4},
  pages = {271-285},
  journal      = {CVGIP: Graphical Models and Image Processing},
}

@article{kay1978,
  author       = {Author(s) Paul Kay and Chad K. Mcdaniel and Paul Kay},
  editor       = {The Pennsylvania State University CiteSeerX Archives},
  title        = {The linguistic significance of the meanings of basic color terms},
  year         = {1978},
  journal      = {Language},
  volume = {54},
  number = {3},
  pages = {610-646},
}

@Article{macdormancowleybelpaeme2009,
  author   = {MacDorman, Karl F. and Cowley, Stephen J. and Belpaeme, Tony},
  title    = {Symbol Grounding},
  journal  = {Symbol Grounding, 2009, 00021},
  year     = {2009},
  note     = {Vielleicht Buch zu Symbol Grounding},
  keywords = {Artificial intelligence, Symbol grounding},
  url      = {http://search.ebscohost.com/login.aspx?direct=true&site=eds-live&db=nlebk&AN=308760&ebv=EB&ppid=},
}

@Comment{jabref-meta: databaseType:bibtex;}
