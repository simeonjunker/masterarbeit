import cv2
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.colors import hsv_to_rgb, rgb_to_hsv
from sklearn.utils.multiclass import unique_labels
import preprocessing

import pylab as pl

def plot_in_hsv_chart(rgb_token, color_term='', alpha=0.5, cmap='Greys_r',xbins=64,ybins=10,figsize=(10,5)):

    fig,ax = plt.subplots(figsize=figsize)

    # 2D-Farbraum (HSV) anzeigen
    #https://stackoverflow.com/questions/10787103/2d-hsv-color-space-in-matplotlib
    V, H = np.mgrid[0:1:100j, 0:1:360j]
    S = np.ones_like(V)
    HSV = np.dstack((H,S,V))
    RGB = hsv_to_rgb(HSV)
    plt.imshow(RGB, origin="lower", extent=[0, 360, 0, 1], aspect=150)

    ax.set_xlabel("H")
    ax.set_ylabel("V")
    ax.set_title('Color term: {color_term}, '.format(color_term=color_term)+"$S_{HSV}=1$")

    # RGB-Werte als Histogramm über Farbraum plotten
    values = np.empty((0,2))

    # RGB in HSV umwandeln und zu values hinzufügen
    for token in rgb_token:
        # RGB-Werte zwischen 0 und 1
        rgb_values = tuple(round(i)/255 for i in token)
        hsv_values = rgb_to_hsv(rgb_values)
        hsv_values[0] = hsv_values[0]*360
        values = np.vstack([values,[hsv_values[0],hsv_values[2]]])

    # 2D-Histogramm mit HSV-Werten aus values
    h = ax.hist2d(values[:,0], values[:,1],bins=[xbins,ybins],range=[[0, 360], [0, 1]], cmap=cmap, alpha=alpha)

    ax.set_xlim(left=0,right=360)
    ax.set_ylim(bottom=0,top=1)

    plt.colorbar(h[3])
    plt.show()

def plot_rgb_color(rgb_token):

    fig,ax = plt.subplots(figsize=(5,5))

    rgb_value = tuple(round(i)/255 for i in rgb_token[:3])
    rect = patches.Rectangle((0,0), 1, 1, color=rgb_value)
    ax.add_patch(rect)

    plt.title("RGB Color Patch: {rgb}".format(rgb=str(tuple(round(i) for i in rgb_token[:3]))))

    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    plt.xlim([0,1])
    plt.ylim([0,1])
    plt.show()

def plot_vg_image(row,img_dir):

    filename = img_dir + str(row.image_id)+'.jpg'
    image = cv2.imread(filename)

    # Bounding Box
    bb ={
        'h' : row.bb_h,
        'w' : row.bb_w,
        'x' : row.bb_x,
        'y' : row.bb_y,
        'label' : row.color+' '+row.object_name
    }

    # Bild plotten
    fig,ax = plt.subplots(figsize=(5,5))
    ax.imshow(np.flip(image,axis=2))
    plt.text(bb['x']+bb['w']+10, bb['y']+bb['h']+10,bb['label'],fontsize=12,color='red',bbox=dict(facecolor='white'))

    # BB hinzufügen
    rect = patches.Rectangle((bb['x'],bb['y']),bb['w'],bb['h'],linewidth=3,edgecolor='green',facecolor='green',alpha=1,fill=False)
    ax.add_patch(rect)

    plt.show()

    try:
        print('predicted color:',row.color_pred)
    except:
        pass
