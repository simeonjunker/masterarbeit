import numpy as np
np.random.seed(123)

import pandas as pd
import sys
import os
import collections
import configparser
from collections import Counter
import matplotlib.pyplot as plt
import configparser
import numpy as np
import os

sys.path.append(os.path.abspath('../Module'))
import preprocessing

config = configparser.ConfigParser()
config.read('../config.ini')
vg_json = config['PATHS']['vg-json']
vg_json_export = config['PATHS']['json-export']
data_dir = vg_json_export + 'feature_arrays/'

colors = preprocessing.basic_colors()
num_classes = 11
random_seed = 123

n = 10000

image_dir = config['PATHS']['vg-images']

train_ids = np.array([])

for file in ['type_to_color', 'baseline_arrays_bgr', 'baseline_arrays_hsv', 'baseline_arrays_lab']:
# beginnen mit type_to_color; da nur frequente Items in diesem Set

    print ('Datei:', file)

    import_arrays = np.load(data_dir+file+'_filtered.npz')

    train_x = import_arrays['train_x']
    train_y = import_arrays['train_y']

    if len(train_ids) == 0:

        train_y_df = pd.DataFrame(train_y[:,1:], columns=colors, index=train_y[:,0])
        train_y = pd.DataFrame(columns=colors)

        for c in colors:
            train_y = train_y.append(train_y_df.loc[train_y_df[c] == 1].sample(n, replace=True, random_state=random_seed))

        train_y = np.append(
            np.array(train_y.index).reshape(-1,1),
            train_y.to_numpy(),
            axis = 1
        )
        np.random.shuffle(train_y)

        train_x = pd.DataFrame(train_x[:,1:], index=train_x[:,0]).loc[train_y[:,0]]
        train_x = np.append(
            np.array(train_x.index).reshape(-1,1),
            train_x.to_numpy(),
            axis = 1
        )

        train_ids = train_y[:,0]

    else:
        train_x_df = pd.DataFrame(train_x[:,1:], index=train_x[:,0]).loc[train_ids]
        train_x = np.append(train_ids.reshape(-1,1), train_x_df.to_numpy(), axis=1)
        train_y_df = pd.DataFrame(train_y[:,1:], index=train_y[:,0]).loc[train_ids]
        train_y = np.append(train_ids.reshape(-1,1), train_y_df.to_numpy(), axis=1)

    outfile = data_dir+file+'_resampled.npz'
    print ('Erzeugte Arrays in Datei schreiben: '+ outfile)
    np.savez_compressed(
        outfile,
        train_x = train_x,
        train_y = train_y,
        dev_x = import_arrays['dev_x'],
        dev_y = import_arrays['dev_y'],
        test_x = import_arrays['test_x'],
        test_y = import_arrays['test_y']
    )
    print ('Shape X:', train_x.shape)
    print ('Shape Y:', train_y.shape)
# save ids to file
outfile = data_dir+'ids.npz'
print ('IDs in Datei schreiben: '+ outfile)
np.savez_compressed(
    outfile,
    train = train_ids,
    dev = import_arrays['dev_y'][:,0],
    test = import_arrays['test_y'][:,0]
)
