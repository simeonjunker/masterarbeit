import pandas as pd
import numpy as np
import os
import sys
import configparser
import collections
import matplotlib.pyplot as plt
from imblearn.under_sampling import RandomUnderSampler
from sklearn.metrics import classification_report, accuracy_score, balanced_accuracy_score

sys.path.append(os.path.abspath('../../Module'))
import preprocessing
import evaluation
import visualize

config = configparser.ConfigParser()
config.read('../../config.ini')

vg_json = config['PATHS']['vg-json']
vg_json_export = config['PATHS']['json-export']
image_dir = config['PATHS']['vg-images']

colors = preprocessing.basic_colors()
num_classes = 11
random_state = 123
max_entries = 10000

if __name__ == "__main__":
    for filename in ['baseline_arrays_bgr']:

        import_arrays = np.load(vg_json_export+filename+'.npz')

        train_x = import_arrays['train_x']
        train_y = import_arrays['train_y']

        #import_arrays = np.load(vg_json_export+filename+'_res.npz')

        dev_x = import_arrays['dev_x']
        dev_y = import_arrays['dev_y']
        test_x = import_arrays['test_x']
        test_y = import_arrays['test_y']

        td_arrays = np.load(vg_json_export+'type_to_color_res.npz')
        td_ids = pd.DataFrame(td_arrays['train_y'][:,0])

        train_x_res = pd.merge(td_ids, pd.DataFrame(train_x), left_on=0, right_on=0)
        train_y_res = pd.merge(td_ids, pd.DataFrame(train_y), left_on=0, right_on=0)

        # Arrays in Datei schreiben
        export_filename = filename+'_res.npz'
        np.savez_compressed(
            vg_json_export+export_filename,
            train_x = train_x_res, train_y = train_y_res,
            test_x = test_x, test_y = test_y,
            dev_x = dev_x, dev_y = dev_y
        )

        print ('Arrays nach Resample:')
        print ('shape train_x:',train_x.shape)
        print ('shape train_y:',train_y.shape)
        print ('shape dev_x:',dev_x.shape)
        print ('shape dev_y:',dev_y.shape)
        print ('shape test_x:',test_x.shape)
        print ('shape test_y:',test_y.shape)
