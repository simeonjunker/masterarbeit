import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os
from ijson import items
import configparser
from sklearn.model_selection import train_test_split
import sys
import re

sys.path.append(os.path.abspath('../Module'))
import preprocessing

config = configparser.ConfigParser()
config.read('../config.ini')

vg_json = config['PATHS']['vg-json']
vg_json_export = config['PATHS']['json-export']
image_dir = config['PATHS']['vg-images']

export_dir = vg_json_export + 'extracted_data/'

test_ratio = 0.1
dev_ratio = 0.2
train_ratio = 1 - (test_ratio + dev_ratio)

def split_name_list(row, new_df):

    image_id = row.image_id
    object_id = row.object_id
    name_list = row.object_name
    attribute = row.color
    bb_h = row.bb_h
    bb_w = row.bb_w
    bb_x = row.bb_x
    bb_y = row.bb_y

    for name in name_list:
        #print (image_id, object_id, name, attribute)
        new_df.append({
            'image_id':image_id,
            'object_id':object_id,
            'object_name':name.lower(),
            'color':attribute.lower(),
            'bb_h':bb_h,
            'bb_w':bb_w,
            'bb_x':bb_x,
            'bb_y':bb_y
        })# , ignore_index=True)
        
def remove_colors_from_names(name, regexp):
    if re.match(regexp, name):
        return (re.sub(regexp,'', name))
    else:
        return (name)

# Grundfarben vgl. Berlin & Kay 1969
basic_colors = preprocessing.basic_colors()

if __name__ == "__main__":
    print ('Objekte mit Farbattribut extrahieren')
    with open (vg_json+'attributes.json', 'r') as f:
        # Import der Objekte
        # -> alle Objekte, die ein Attribut aus basic_colors haben
        # -> Farbbezeichnung wird normiert (durch lower() )
        out = []
        for entry in items(f, 'item'):
            image_id = entry['image_id']
            for attributes in entry['attributes']:
                out_obj = {}
                object_id = attributes.get('object_id', None)
                object_name = attributes.get('names', None)
                w = attributes.get('w',None)
                h = attributes.get('h',None)
                x = attributes.get('x',None)
                y = attributes.get('y',None)
                if attributes.get('attributes', None) != None:
                    for attribute in attributes['attributes']:
                        if attribute.lower() in basic_colors:
                            out_obj = {
                                'image_id': image_id,
                                'object_id': object_id,
                                'object_name': object_name,
                                'color': attribute.lower(),
                                'bb_w': w,
                                'bb_h': h,
                                'bb_x': x,
                                'bb_y': y
                                }
                            out.append(out_obj)
                            out_obj = {}

    # als DataFrame speichern
    objects = pd.DataFrame.from_dict(out)
    print ("DataFrame Shape:",objects.shape)

    # Einträge teilen, falls mehrere Bezeichnungen für ein Objekt
    print ("Einträge teilen, falls mehrere Bezeichnungen für ein Objekt")
    objects_entries = []
    objects.apply(lambda x:split_name_list(x, objects_entries),axis=1)
    objects = pd.DataFrame.from_dict(objects_entries)
    print ("DataFrame Shape:",objects.shape)

    # Farbbezeichnungen aus Objektnamen entfernen
    print ('Farbbezeichnungen aus Objektnamen entfernen')
    regexp = r'\b(black|white|red|green|yellow|blue|brown|orange|pink|purple|gray|grey)( |\b)'
    objects.object_name = objects.apply(lambda x: remove_colors_from_names(x.object_name, regexp), axis=1)
    
    if not os.path.isdir(export_dir):
        print ('create export directory')
        os.mkdir(export_dir)

    # als CSV exportieren
    print ("DataFrame als CSV exportieren")
    export = objects.to_csv(export_dir+"all_objects.csv")

    # Train/Test/Dev-Split
    n_entries = len(objects)

    test_size = int(n_entries * test_ratio)
    dev_size = int(n_entries * dev_ratio)
    train_size = n_entries - (test_size + dev_size)

    print ("Split in Train/Test/Dev-Sets")
    train_df,test_df = train_test_split(objects, test_size=test_size, shuffle=True, random_state=123)
    train_df,dev_df = train_test_split(train_df, test_size=dev_size, shuffle=True, random_state=123)

    print (
    """
    Train-Set: {train_size} Einträge ({train_ratio})
    Test-Set: {test_size} Einträge ({test_ratio})
    Dev-Set: {dev_size} Einträge ({dev_ratio})
    """.format( train_size = train_size, train_ratio = train_ratio,
                test_size = test_size, test_ratio = test_ratio,
                dev_size = dev_size, dev_ratio = dev_ratio
            )
    )

    # Train/Test-Splits als CSV exportieren
    export  = train_df.to_csv(export_dir+"train_df.csv")
    export  = test_df.to_csv(export_dir+"test_df.csv")
    export  = dev_df.to_csv(export_dir+"dev_df.csv")
