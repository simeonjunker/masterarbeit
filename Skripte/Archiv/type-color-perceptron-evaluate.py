'''Trains a simple deep NN on the MNIST dataset.
Gets to 98.40% test accuracy after 20 epochs
(there is *a lot* of margin for parameter tuning).
2 seconds per epoch on a K520 GPU.
'''

import keras
from keras.datasets import mnist
from keras.models import Sequential, load_model
from keras.layers import Dense, Dropout
from keras.optimizers import RMSprop
import configparser
import numpy as np

config = configparser.ConfigParser()
config.read('../../config.ini')

data_dir = config['PATHS']['json-export']

batch_size = 128
num_classes = 11
epochs = 25

model = load_model(data_dir+'type-color-perceptron.h5')

# All
import_arrays = np.load(data_dir+"type_to_color.npz")
# exclude 1st column from every array (contains id)

test_x = import_arrays['test_x']
test_y = import_arrays['test_y']
x_test = test_x[:,1:]
y_test = test_y[:,1:]

print ('input shapes: test_x: {a}, test_y: {b}'.format(a=x_test.shape, b=y_test.shape))

print ('\n\nAll Objects')
test_predict = model.predict(x_test)
score = model.evaluate(x_test, y_test, verbose=0)
print('Test loss:', score[0])
print('Test accuracy:', score[1])

np.savez_compressed(data_dir+"results_type_to_color.npz",
    test_x = test_x,
    test_y = test_y,
    test_predict = test_predict
)
