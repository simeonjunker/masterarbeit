import numpy as np
np.random.seed(123)

import keras
from keras.layers import Input, Dense, Dropout,Embedding,concatenate,Flatten
from keras.models import Model
from keras.utils.vis_utils import model_to_dot
from keras.optimizers import RMSprop
from keras.utils import plot_model
import configparser
import os
import pandas as pd

config = configparser.ConfigParser()
config.read('../../config.ini')

data_dir = config['PATHS']['json-export']
input_dir = data_dir+'feature_arrays/'
output_dir = data_dir + 'models/'
batch_size = 128
num_classes = 11
epochs = 25

if not os.path.isdir(output_dir):
    os.mkdir(output_dir)
    print ('Pfad {path} angelegt'.format(path=output_dir))

# Gezippte Numpy-Archive importieren
bottomup_arrays = np.load(input_dir+'baseline_arrays_bgr_resampled.npz', allow_pickle=True)
topdown_arrays = np.load(input_dir+'type_to_color_resampled.npz', allow_pickle=True)

train_bu_x = bottomup_arrays['train_x']
train_td_x = topdown_arrays['train_x']
train_y = topdown_arrays['train_y']
train_ids = topdown_arrays['train_y'][:,0]

dev_bu_x = bottomup_arrays['dev_x']
dev_td_x = topdown_arrays['dev_x']
dev_y = topdown_arrays['dev_y']
dev_ids = topdown_arrays['dev_y'][:,0]

dev_bu_x = pd.DataFrame(dev_bu_x, index=dev_bu_x[:,0]).loc[dev_ids].to_numpy()

print ('Train-IDs identisch:', np.array_equal(train_bu_x[:,0], train_td_x[:,0]))
print ('Dev-IDs identisch:', np.array_equal(dev_bu_x[:,0], dev_td_x[:,0]))
    
    
# Inputs
bu_inputs = Input(shape=(train_bu_x.shape[1]-1,), name='bu_input')
td_inputs = Input(shape=(1,), name='td_input')

td_embedding = Embedding(train_td_x[:,1:].shape[1], 6, input_length=1, name='td_embedding')(td_inputs)
flatten = Flatten(name='td_flatten')(td_embedding)

conc = concatenate([bu_inputs, flatten],axis=1, name='bu_td_concat')

x = Dense(240, activation='relu')(conc)
x = Dropout(0.2)(x)

x = Dense(24, activation='relu')(x)
x = Dropout(0.2)(x)

predictions = Dense(num_classes, activation='softmax', name='predictions')(x)

model = Model(inputs=[bu_inputs,td_inputs], outputs=predictions)

model.compile(loss='categorical_crossentropy',
              optimizer=RMSprop(),
              metrics=['accuracy'])

plot_model(model, to_file=data_dir+'images/earlyfusion_model_plot.png', show_shapes=True, show_layer_names=False)
    
model.summary()

history = model.fit([train_bu_x[:,1:], train_td_x[:,1:].argmax(axis=1)], train_y[:,1:],
                    batch_size=batch_size,
                    epochs=epochs,
                    verbose=1,
                    validation_data=([dev_bu_x[:,1:], dev_td_x[:,1:].argmax(axis=1)], dev_y[:,1:]))
score = model.evaluate([dev_bu_x[:,1:], dev_td_x[:,1:].argmax(axis=1)], dev_y[:,1:], verbose=0)

print('Test loss:', score[0])
print('Test accuracy:', score[1])

model_file = 'earlyfusion_model.h5'
print ('saving model to', model_file)
model.save(output_dir+model_file)
