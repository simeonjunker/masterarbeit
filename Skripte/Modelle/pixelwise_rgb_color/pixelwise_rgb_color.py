import pandas as pd
import numpy as np
import sys
import os
import configparser
import skimage

sys.path.append(os.path.abspath('../../Module'))
import preprocessing

config = configparser.ConfigParser()
config.read('../../config.ini')
vg_json = config['PATHS']['vg-json']
data_dir = config['PATHS']['json-export']
image_dir = config['PATHS']['vg-images']
model_dir = model_input_dir = config['PATHS']['data-repo']+'BottomUp_Klassifikator_Vergleich/Modelle/'
output_dir = data_dir+'prediction_arrays/'

if not os.path.isdir(output_dir):
    os.mkdir(output_dir)
    print ('Pfad {path} angelegt'.format(path=output_dir))

source_dir = data_dir+'extracted_data/'
w2c_datasets = np.load(model_dir+'pixelwise_lookup_tables.npz')

def color_pixelwise(img, w2c, bb=False):
    # vgl. Datei im2c.m aus http://lear.inrialpes.fr/people/vandeweijer/code/ColorNaming.tar
    # Input: Bild-Pfad und w2c-Matrix (Mapping von RGB-Werten zu Farbnamen)

    # Bild als Array
    img = skimage.io.imread(img)

    # für Grayscale-Bilder
    if len(img.shape) < 3:
        img = skimage.color.gray2rgb(img, alpha=None)

    # img-Array slicen, falls Bounding Box gegeben
    if bb:
        if type(bb) == list:
            bb ={
                'h' : bb[0],
                'w' : bb[1],
                'x' : bb[2],
                'y' : bb[3]
            }
        img = img[bb['y']:bb['y']+bb['h'], bb['x']:bb['x']+bb['w']]

    # Kanäle trennen
    RR=img[:,:,0]
    GG=img[:,:,1]
    BB=img[:,:,2]

    # Index-Array erstellen: Ein Index-Wert pro Pixel
    index_img = np.array(
        # R-Werte (32 Bins)
        np.floor(RR/8)+
        # G-Werte (32 Bins)
        32* np.floor(GG/8)+
        # B-Werte (32 Bins)
        32*32*np.floor(BB/8)
    )
    # Array für Wahrscheinlichkeitsverteilung über Farbklassen initialisieren
    clr_distribution = np.zeros(11)

    # Für jeden Index-Wert die jeweils assoziierte Wahrscheinlichkeitsverteilung zu clr_distribution addieren
    for pxl_index in index_img.ravel():
        clr_distribution = clr_distribution + w2c[int(pxl_index)][3:]

    # clr_distribution normalisieren
    clr_distribution = clr_distribution / len(index_img.ravel())

    return clr_distribution

def rows_pixelwise_classification(row,  w2c):
    bb = {
        'h' : row.bb_h,
        'w' : row.bb_w,
        'x' : row.bb_x,
        'y' : row.bb_y
    }
    img_path = image_dir + str(row.image_id) + '.jpg'
    return (np.append(row.Index, color_pixelwise(img_path, w2c, bb)))

def dataframe_pixelwise_classification(df, w2c):
    results = np.empty((0,12))
    for row in df.itertuples():
        res = rows_pixelwise_classification(row, w2c)
        res = np.reshape(res, (1,12))
        results = np.append(results, res, axis=0)
        print (results.shape[0], '/', len(df))
    return results

if __name__ == "__main__":

    # DataFrames für Test/Dev-Splits laden
    test_df = pd.read_csv(source_dir+"test_df.csv", index_col=0)
    dev_df = pd.read_csv(source_dir+"dev_df.csv", index_col=0)

    print ('Test set shape:', test_df.shape)
    print ('Dev set shape:', dev_df.shape)

    print ('Starting Classification')
    dev_w2c = dataframe_pixelwise_classification(dev_df, w2c_datasets['w2c'])
    test_w2c = dataframe_pixelwise_classification(test_df, w2c_datasets['w2c'])
    dev_chip_w2c = dataframe_pixelwise_classification(dev_df, w2c_datasets['chip_w2c'])
    test_chip_w2c = dataframe_pixelwise_classification(test_df, w2c_datasets['chip_w2c'])

    # Als Datei exportieren
    print ('write numpy arrays to file')

    export_filename = 'pixelwise_rgb_color.npz'
    np.savez_compressed(
        output_dir+export_filename,
        test_w2c = test_w2c,
        dev_w2c = dev_w2c,
        test_chip_w2c = test_chip_w2c,
        dev_chip_w2c = dev_chip_w2c
    )

    print ('shapes:')
    print ('test_w2c:', test_w2c.shape)
    print ('dev_w2c:', dev_w2c.shape)
    print ('test_chip_w2c', test_chip_w2c.shape)
    print ('dev_chip_w2c', dev_chip_w2c.shape)
