import pandas as pd
import numpy as np
import configparser
import os
from joblib import dump, load
from sklearn.naive_bayes import BernoulliNB

config = configparser.ConfigParser()
config.read('../../config.ini')
data_dir = config['PATHS']['json-export']
import_dir = data_dir+'feature_arrays/'
#model_dir = config['PATHS']['data-repo']+'BekannteKlassen_LateFusion/Modelle/'
model_dir = data_dir + 'models/'
output_dir = data_dir+'prediction_arrays/'

print ('loading model from ' + model_dir)
classifier = load(model_dir+'topdown_naive_bayes.joblib')

if not os.path.isdir(output_dir):
    os.mkdir(output_dir)
    print ('Pfad {path} angelegt'.format(path=output_dir))

print ('input-arrays importieren')
import_arrays = np.load(import_dir+'type_to_color_resampled.npz')

test_x = import_arrays['test_x']
print ('test_x:',test_x.shape)
test_y = import_arrays['test_y']
print ('test_y:',test_y.shape)
dev_x = import_arrays['dev_x']
print ('dev_x:',dev_x.shape)
dev_y = import_arrays['dev_y']
print ('dev_y:',dev_y.shape)

print ('arrays formatieren')
# Indizes abtrennen
dev_y_index = dev_y[:,0:1]
dev_y = dev_y[:,1:]
test_y_index = test_y[:,0:1]

dev_x = dev_x[:,1:]
test_x = test_x[:,1:]

# One-Hot-Kodierung -> Int
dev_y = dev_y.argmax(axis=1)

score_all = classifier.score(dev_x,dev_y)
# Prediction für Dev- und Test-Sets
pred_dev_y = classifier.predict_proba(dev_x)
pred_test_y = classifier.predict_proba(test_x)
print ('score:', score_all)
print ('predictions dev:',pred_dev_y.shape)
print ('predictions test:',pred_test_y.shape)

# In Datei schreiben
np.savez_compressed(output_dir+"results_type_to_color.npz",
    test_y = import_arrays['test_y'],
    dev_y = import_arrays['dev_y'],
    pred_test_y = pred_test_y,
    pred_dev_y = pred_dev_y
)
